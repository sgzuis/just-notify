import React from 'react';
import {StatusBar} from 'expo-status-bar';
import * as Notifications from 'expo-notifications';
import {StyleSheet, Text, View} from 'react-native';
import {registerForPushNotificationsAsync} from "./Components/registerForPushNotificationsAsync";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default class App extends React.Component {
  state = {
    notification: {},
    pushToken: null,
  };

  componentDidMount() {
    registerForPushNotificationsAsync()
      .then(token => {
        this.setState({
          pushToken: token
        })
      });

    Notifications.addNotificationReceivedListener(this._handleNotification);

    Notifications.addNotificationResponseReceivedListener(this._handleNotificationResponse);
  }

  _handleNotification = notification => {
    this.setState({ notification: notification });
  };

  _handleNotificationResponse = response => {
    console.log(response);
  };

  render() {
    if (!this.state?.pushToken) {
      return null;
    }

    console.log(this.state?.pushToken)

    return (
      <View style={styles.container}>
        <StatusBar style="auto"/>
        <Text>Your expo push token: {this.state?.pushToken}</Text>
        {
          this.state.notification.length ? (
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <Text>Title: {this.state.notification?.request.content.title}</Text>
              <Text>Body: {this.state.notification?.request.content.body}</Text>
              <Text>Data: {JSON.stringify(this.state?.notification.request.content.data)}</Text>
            </View>
          ) : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
